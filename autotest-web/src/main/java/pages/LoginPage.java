package pages;

import com.codeborne.selenide.SelenideElement;
import ru.lanit.at.web.annotations.Name;
import ru.lanit.at.web.pagecontext.WebPage;

import static com.codeborne.selenide.Selenide.$x;

@Name(value = "LoginPage")
public class LoginPage extends WebPage {

    @Name("поле username")
    private SelenideElement usernameField = $x("//input[@id='username']");

    @Name("поле password")
    private SelenideElement passwordField = $x("//input[@id='password']");

    @Name("Логин")
    private SelenideElement loginButton = $x("//input[@value='Login']");

}