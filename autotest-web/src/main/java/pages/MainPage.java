package pages;

import com.codeborne.selenide.SelenideElement;
import ru.lanit.at.web.annotations.Name;
import ru.lanit.at.web.pagecontext.WebPage;

import static com.codeborne.selenide.Selenide.$x;

@Name(value = "Helpdesk")
public class MainPage extends WebPage {

    @Name("поле поиска")
    private SelenideElement searchField = $x("//input[@id='search_query']");

    @Name("Поиск")
    private SelenideElement searchButton = $x("//button/i[contains(@class,'fa-search')]");

    @Name("Логин")
    private SelenideElement loginButton = $x("//a[@href='/login/?next=/']");

}