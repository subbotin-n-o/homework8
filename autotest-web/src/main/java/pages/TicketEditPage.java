package pages;

import com.codeborne.selenide.SelenideElement;
import ru.lanit.at.web.annotations.Name;
import ru.lanit.at.web.pagecontext.WebPage;

import static com.codeborne.selenide.Selenide.$x;

@Name(value = "TicketEditPage")
public class TicketEditPage extends WebPage {

    @Name("Поле описание")
    private SelenideElement descriptionField = $x("//textarea[@id='id_description']");

    @Name("Сохранить")
    private SelenideElement saveButton = $x("//input[@type='submit']");

}
