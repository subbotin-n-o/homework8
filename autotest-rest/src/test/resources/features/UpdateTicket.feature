#language:ru
@test
Функционал: Тест сервиса Helpdesk
  - Негативная проверка: перевод статуса тикета из "Закрыт" в "Открыт".

  Сценарий: Создание нового тикета с высоким приоритетом.
            Перевод статуса тикета из "Закрыт" в "Открыт".

    # Второй тест - Негативная проверка: перевода статуса тикета из "Закрыт" в "Открыт".
    # Генерится рандомная строка по маске
        # E - Английская буква,
        # R - русская буква,
        # D - цифра. Остальные символы игнорятся
        # Условна дана строка TEST_EEE_DDD_RRR - снегерится примерно такая - TEST_QRG_904_ЙЦУ
    * сгенерировать переменные
      | id              | 0                      |
      | title           | EEE_DDD_RRR            |
      | queue           | 1                      |
      | status          | 2                      |
      | submitter_email | subbotin.n.o@gmail.com |

    # Создаем тикет
    * создать запрос
      | method | path         | body              |
      | POST   | /api/tickets | createTicket.json |

    * добавить header
      | Accept       | application/json |
      | Content-Type | application/json |

    * отправить запрос
    * статус код 201
    * извлечь данные
      | id | $.id |

    * сгенерировать переменные
      | status | 4 |

    # Авторизация
    * создать запрос
      | method | path       | body           |
      | POST   | /api/login | loginUser.json |

    * добавить header
      | Accept       | application/json |
      | Content-Type | application/json |

    * отправить запрос
    * статус код 200
    * извлечь данные
      | token | $.token |

    # Перевод статуса из "Закрыт" в "Открыт"
    * создать запрос
      | method | path               | body              |
      | PUT    | /api/tickets/${id} | createTicket.json |

    * добавить header
      | Authorization | token ${token}   |
      | Accept        | application/json |
      | Content-Type  | application/json |

    * отправить запрос
    * статус код 422