#language:ru
@test
Функционал: Тест сервиса Helpdesk
  - Создание нового тикета с высоким приоритетом.

  Сценарий: Создание нового тикета с высоким приоритетом.

    # Первый тест - Создание нового тикета с высоким приоритетом.
    # Генерится рандомная строка по маске
        # E - Английская буква,
        # R - русская буква,
        # D - цифра. Остальные символы игнорятся
        # Условна дана строка TEST_EEE_DDD_RRR - снегерится примерно такая - TEST_QRG_904_ЙЦУ

    * сгенерировать переменные
      | id              | 0                      |
      | title           | EEE_DDD_RRR            |
      | queue           | 1                      |
      | status          | 2                      |
      | submitter_email | subbotin.n.o@gmail.com |

    # Создаем тикет
    * создать запрос
      | method | path         | body              |
      | POST   | /api/tickets | createTicket.json |

    * добавить header
      | Accept       | application/json |
      | Content-Type | application/json |

    * отправить запрос
    * статус код 201
    * извлечь данные
      | id | $.id |